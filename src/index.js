import * as React from 'react';
import { Component } from 'react-simplified';
import ReactDOM from 'react-dom';
import { NavLink, HashRouter, Route } from 'react-router-dom';
import { combineService, studentService, subjectService } from './services';

import createHashHistory from 'history/createHashHistory';
const history = createHashHistory(); // Use history.push(...) to programmatically change path, for instance after successfully saving a student

// class Menu extends Component {
//   render() {
//     return (
//       <div>
//         <NavLink to="/students">Students</NavLink>
//       </div>
//     );
//   }
// }

class Tabs extends Component {

  render() {
    return (
      <div>
        <ul className="nav nav-tabs">
          <li className="nav-item">
            <NavLink className="nav-link" to={"/whiteboard"}>Whiteboard</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to={"/students"}>Students</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to={"/subjects"}>Subjects</NavLink>
          </li>
        </ul>
      </div>
    )
  }
}

class StudentList extends Component {
  students = [];

  render() {
    return (
      <div style={{border: "solid black", backgroundColor: "darkgrey"}}>
        <h5>Students</h5>
        <ul>
          {this.students.map(student => (
            <li key={student.id}>
              <NavLink to={'/students/details/' + student.id}>{student.name}</NavLink>
            </li>
          ))}
        </ul>
        <div>
          <h6>Options</h6>
          <ul>
            <li>
              <NavLink to={"/students/create"}>Add new student</NavLink>
            </li>
          </ul>
        </div>
      </div>
    );
  }

  mounted() {
    studentService.getStudents(students => {
      this.students = students;
    });
  }
}

class StudentDetails extends Component {
  name = '';
  email = '';
  subjects = [];

  render() {
    return (
      <div style={{ border: "solid black", backgroundColor: "pink", color: "teal" }}>
        <h5>Details</h5>
        <ul>
          <li>Name: {this.name}</li>
          <li>Email: {this.email}</li>
          <li>Subjects:
            <ul>
              {this.subjects.map(subject => (
                <li key={subject.id}>{subject.name}</li>
              ))}
            </ul>
          </li>
        </ul>
        <div>
          <h6>Options</h6>
          <ul>
            <li>
              <NavLink to={"/students/details/" + this.props.match.params.id + "/edit"}>Edit student</NavLink>
            </li>
            <li>
              <NavLink to={"/students/details/" + this.props.match.params.id + "/remove"}>Remove student</NavLink>
            </li>
            <li>
              <NavLink to={"/students/details/" + this.props.match.params.id +  "/add_subject"}>New subject</NavLink>
            </li>
            <li>
              <NavLink to={"/students/details/" + this.props.match.params.id +  "/remove_subject"}>Remove subject</NavLink>
            </li>
          </ul>
        </div>
      </div>
    );
  }

  mounted() {
    studentService.getStudent(this.props.match.params.id, student => {
      this.name = student.name;
      this.email = student.email;
    });
    studentService.getSubjects(this.props.match.params.id, subjects => {
      this.subjects = subjects;
    });
  }
}

class StudentEdit extends Component {
  name = '';
  email = '';

  render() {
    return (
      <form>
        Name: <input type="text" value={this.name} onChange={event => (this.name = event.target.value)} />
        Email: <input type="text" value={this.email} onChange={event => (this.email = event.target.value)} />
        <button type="button" onClick={this.save}>
          Save
        </button>
      </form>
    );
  }

  mounted() {
    studentService.getStudent(this.props.match.params.id, student => {
      this.name = student.name;
      this.email = student.email;
    });
  }

  save() {
    studentService.updateStudent(this.props.match.params.id, this.name, this.email, () => {
      history.push('/students');
    });
  }
}

class StudentAdd extends Component {
  name="";
  email="";

  render() {
    return(
      <div>
        <form>
          Name: <input type="text" onChange={event => (this.name = event.target.value)} />
          Email: <input type="text" onChange={event => (this.email = event.target.value)} />
          <button type="button" onClick={this.add}>Add</button>
        </form>
      </div>
    )
  }

  add() {
    studentService.addStudent(this.name, this.email, () => {
      history.push("/students");
    });
  }
}

class StudentDelete extends Component {
  name = "";

  render() {
    return(
      <div>
        <h6>Are you sure you want to remove {this.name}?</h6>
        <form>
          <button type="submit" onClick={this.delete}>Yes</button>
          <button type="submit" onClick={this.home}>No</button>
        </form>
      </div>
    )
  }

  mounted() {
    studentService.getStudent(this.props.match.params.id, student => {
      this.name = student.name;
    });
  }

  home() {
    history.push("/students");
  }

  delete() {
    studentService.deleteStudent(this.props.match.params.id, () => {
      this.home();
    });
  }
}

class SubjectList extends Component {
  subjects = [];

  render() {
    return (
      <div style={{border: "solid black", backgroundColor: "darkgrey"}}>
        <h5>Subjects</h5>
        <ul>
          {this.subjects.map(subject => (
            <li key={subject.id}>
              <NavLink to={'/subjects/details/' + subject.id}>{subject.name}</NavLink>
            </li>
          ))}
        </ul>
        <div>
          <h6>Options</h6>
          <ul>
            <li>
              <NavLink to={"/subjects/create"}>Add new subject</NavLink>
            </li>
          </ul>
        </div>
      </div>
    );
  }

  mounted() {
    subjectService.getSubjects(subjects => {
      this.subjects = subjects;
    });
  }
}


class SubjectDetails extends Component {
  name = "";
  code = "";
  students = [];

  render() {
    return (
      <div style={{ border: "solid black", backgroundColor: "pink", color: "teal" }}>
        <h5>Details</h5>
        <ul>
          <li>Code: {this.code}</li>
          <li>Name: {this.name}</li>
          <li>Students who take this subject:
            <ul>
              {this.students.map(student => (
                <li key={student.id}>{student.name}</li>
              ))}
            </ul>
          </li>
        </ul>
        <div>
          <h6>Options</h6>
          <ul>
            <li>
              <NavLink to={"/subjects/details/" + this.props.match.params.id + "/edit"}>Edit subject</NavLink>
            </li>
            <li>
              <NavLink to={"/subjects/details/" + this.props.match.params.id + "/remove"}>Remove subject</NavLink>
            </li>
          </ul>
        </div>
      </div>
    );
  }

  mounted() {
    subjectService.getSubject(this.props.match.params.id, subject => {
      this.name = subject.name;
      this.code = subject.code;
    });
    subjectService.getStudents(this.props.match.params.id, students => {
      this.students = students;
    });
  }
}

class SubjectEdit extends Component {
  code = '';
  name = '';

  render() {
    return (
      <form>
        Code: <input type="text" value={this.code} onChange={event => (this.code = event.target.value)} />
        Name: <input type="text" value={this.name} onChange={event => (this.name = event.target.value)} />
        <button type="button" onClick={this.save}>
          Save
        </button>
      </form>
    );
  }

  mounted() {
    subjectService.getSubject(this.props.match.params.id, subject => {
      this.code = subject.code;
      this.name = subject.name;
    });
  }

  save() {
    subjectService.updateSubject(this.props.match.params.id, this.code, this.name, () => {
      history.push('/subjects');
    });
  }
}

class SubjectAdd extends Component {
  code="";
  name="";

  render() {
    return(
      <div>
        <form>
          Code: <input type="text" onChange={event => (this.code = event.target.value)} />
          Name: <input type="text" onChange={event => (this.name = event.target.value)} />
          <button type="button" onClick={this.add}>Add</button>
        </form>
      </div>
    )
  }

  add() {
    subjectService.addSubject(this.code, this.name, () => {
      history.push("/subjects");
    });
  }
}

class SubjectDelete extends Component {
  code = "";
  name = "";

  render() {
    return(
      <div>
        <h6>Are you sure you want to remove {this.code} {this.name} ?</h6>
        <form>
          <button type="submit" onClick={this.delete}>Yes</button>
          <button type="submit" onClick={this.home}>No</button>
        </form>
      </div>
    )
  }

  mounted() {
    subjectService.getSubject(this.props.match.params.id, subject => {
      this.code = subject.code;
      this.name = subject.name;
    });
  }

  home() {
    history.push("/subjects");
  }

  delete() {
    subjectService.deleteSubject(this.props.match.params.id, () => {
      this.home();
    });
  }
}

class SignUp extends Component {
  availableSubjects = [];
  selectedSubjects = [];
  full = false;

  render() {
    if (!this.full) {
      return (
        <div>
          <form>
            <ul>
              {this.availableSubjects.map(subject => (
                <li key={subject.id}>
                  <label htmlFor={subject.id}>{subject.name + " " + subject.code}</label>
                  <input type="checkbox" key={subject.id} id={subject.id}
                         onChange={event => this.handleCheck(event)}/>
                </li>
              ))}
            </ul>
            <button type="submit" onClick={this.signUp}>Sign up</button>
          </form>
        </div>
      )
    } else {
      return(
        <div>
          <h6>Student is already taking all the courses</h6>
          <p>Redirecting home...</p>
        </div>
      )
    }
  }

  handleCheck(event) {
    if (event.target.checked) {
      this.selectedSubjects.push(event.target.id);
    } else {
      let index = this.selectedSubjects.indexOf(event.target.id);
      console.log(index);
      if (index > -1) {
        this.selectedSubjects.splice(index, 1);
      }
    }
    console.log(this.selectedSubjects);
  }

  signUp() {
    for (let i = 0; i < this.selectedSubjects.length; i++) {
      let subjId = this.selectedSubjects[i];
      combineService.addStudentSubject(this.props.match.params.id, subjId, () => {
      });
    }
    history.push("/students/details/" + this.props.match.params.id);
  }

  mounted() {
    combineService.getAvailableSubjects(this.props.match.params.id, subjects => {
      this.availableSubjects = subjects;
      if (subjects.length === 0) {
        this.full = true;
        setTimeout(function() {
          history.push("/students");
        }, 3000);
      }
    });
  }
}

class Resign extends Component {
  subjects = [];
  selectedSubjects = [];
  empty = false;

  render() {
    if (!this.empty) {
      return (
        <div>
          <form>
            <ul>
              {this.subjects.map(subject => (
                <li key={subject.id}>
                  <label htmlFor={subject.id}>{subject.name + " " + subject.code}</label>
                  <input type="checkbox" key={subject.id-1} id={subject.id}
                         onChange={event => this.handleCheck(event)}/>
                </li>
              ))}
            </ul>
            <button type="submit" onClick={this.resign}>Resign</button>
          </form>
        </div>
      )
    } else {
      return(
        <div>
          <h6>Student is not taking any subjects</h6>
          <p>Redirecting home...</p>
        </div>
      )
    }
  }

  handleCheck(event) {
    if (event.target.checked) {
      this.selectedSubjects.push(event.target.id);
    } else {
      let index = this.selectedSubjects.indexOf(event.target.id);
      console.log(index);
      if (index > -1) {
        this.selectedSubjects.splice(index, 1);
      }
    }
    console.log(this.selectedSubjects);
  }

  resign() {
    for (let i = 0; i < this.selectedSubjects.length; i++) {
      let subjId = this.selectedSubjects[i];
      combineService.deleteStudentSubject(this.props.match.params.id, subjId, () => {
      });
    }
    history.push("/students/details/" + this.props.match.params.id);
  }

  mounted() {
    studentService.getSubjects(this.props.match.params.id, subjects => {
      this.subjects = subjects;
      if (subjects.length === 0) {
        this.empty = true;
        setTimeout(function() {
          history.push("/students");
        }, 3000);
      }
    });
  }
}

ReactDOM.render(
  <HashRouter>
    <div>
      <Tabs />
      <Route exact path="/students" component={StudentList} />
      <Route exact path="/subjects" component={SubjectList} />
      <Route exact path="/students/details/:id" component={StudentDetails} />
      <Route exact path="/subjects/details/:id" component={SubjectDetails} />
      <Route exact path="/students/create" component={StudentAdd} />
      <Route exact path="/subjects/create" component={SubjectAdd} />
      <Route path="/students/details/:id/edit" component={StudentEdit} />
      <Route path="/subjects/details/:id/edit" component={SubjectEdit} />
      <Route path="/students/details/:id/remove" component={StudentDelete} />
      <Route path="/subjects/details/:id/remove" component={SubjectDelete} />
      <Route path="/students/details/:id/add_subject" component={SignUp} />
      <Route path="/students/details/:id/remove_subject" component={Resign} />
    </div>
  </HashRouter>,
  document.getElementById('root')
);
