import { connection } from './mysql_connection';

class StudentService {
  getStudents(success) {
    connection.query('select * from Students', (error, results) => {
      if (error) return console.error(error);

      success(results);
    });
  }

  getStudent(id, success) {
    connection.query('select * from Students where id=?', [id], (error, results) => {
      if (error) return console.error(error);

      success(results[0]);
    });
  }

  updateStudent(id, name, email, success) {
    connection.query('update Students set name=?, email=? where id=?', [name, email, id], (error, results) => {
      if (error) return console.error(error);

      success();
    });
  }

  addStudent(name, email, success) {
    let sql = "insert into Students (name, email) values (?, ?)";
    connection.query(sql, [name, email], (error, results) => {
      if (error) return console.error(error);

      success();
    });
  }

  deleteStudent(id, success) {
    let sql = "delete from Students where id=?";
    connection.query(sql, [id], (error, results) => {
      if (error) return console.error(error);

      success();
    });
  }

  getSubjects(id, success) {
    let sql = "select S.id, S.code, S.name\n" +
      "from Students\n" +
      "inner join Student_has_subject Shs on Students.id = Shs.stud_id\n" +
      "inner join Subjects S on Shs.subj_id = S.id\n" +
      "where Students.id = ?";
    connection.query(sql, [id], (error, results) => {
      if (error) return console.error(error);

      success(results);
    });
  }
}
export let studentService = new StudentService();

class SubjectService {
  getSubjects(success) {
    connection.query('select * from Subjects', (error, results) => {
      if (error) return console.error(error);

      success(results);
    });
  }

  getSubject(id, success) {
    connection.query('select * from Subjects where id=?', [id], (error, results) => {
      if (error) return console.error(error);

      success(results[0]);
    });
  }

  updateSubject(id, code, name, success) {
    connection.query('update Subjects set code=?, name=? where id=?', [code, name, id], (error, results) => {
      if (error) return console.error(error);

      success();
    });
  }

  addSubject(code, name, success) {
    let sql = "insert into Subjects (code, name) values (?, ?)";
    connection.query(sql, [code, name], (error, results) => {
      if (error) return console.error(error);

      success();
    });
  }

  deleteSubject(id, success) {
    let sql = "delete from Subjects where id=?";
    connection.query(sql, [id], (error, results) => {
      if (error) return console.error(error);

      success();
    });
  }

  getStudents(id, success) {
    let sql = "select Students.id, Students.name\n" +
      "from Students\n" +
      "inner join Student_has_subject Shs on Students.id = Shs.stud_id\n" +
      "inner join Subjects S on Shs.subj_id = S.id\n" +
      "where S.id = ?";
    connection.query(sql, [id], (error, results) => {
      if (error) return console.error(error);

      success(results)
    });
  }
}

export let subjectService = new SubjectService();

class CombineTablesService {

  getAvailableSubjects(id, success) {
    let sql = "select id, code, name\n" +
      "from Subjects\n" +
      "where id not in (\n" +
      "  select S2.id\n" +
      "  from Students S\n" +
      "  inner join Student_has_subject Shs on S.id = Shs.stud_id\n" +
      "  inner join Subjects S2 on Shs.subj_id = S2.id\n" +
      "  where S.id = ?\n" +
      "  )\n" +
      "and name not in (\n" +
      "  select S2.name\n" +
      "  from Students S\n" +
      "  inner join Student_has_subject Shs on S.id = Shs.stud_id\n" +
      "  inner join Subjects S2 on Shs.subj_id = S2.id\n" +
      "  where S.id = ?\n" +
      "  )\n" +
      "and code not in (\n" +
      "  select S2.code\n" +
      "  from Students S\n" +
      "  inner join Student_has_subject Shs on S.id = Shs.stud_id\n" +
      "  inner join Subjects S2 on Shs.subj_id = S2.id\n" +
      "  where S.id = ?\n" +
      ")";
    connection.query(sql, [id, id, id], (error, results) => {
      if (error) return console.error(error);

      success(results);
    });
  }

  addStudentSubject(studId, subjId, success) {
    let sql = "insert into Student_has_subject (stud_id, subj_id) VALUES (?, ?)";
    connection.query(sql, [studId, subjId], (error, results) => {
      if (error) return console.error(error);

      success(results);
    });
  }

  deleteStudentSubject(studId, subjId, success) {
    let sql = "delete from Student_has_subject where stud_id = ? and subj_id = ?";
    connection.query(sql, [studId, subjId, (error, results) => {
      if (error) return console.error(error);

      success(results);
    }]);
  }
}

export let combineService = new CombineTablesService();